<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/16/18
 * Time: 9:53 PM
 */

namespace App\Model\User;

use App\Entity\User;

class UserHandler
{
    /**
     * @param array $data
     * @return User
     */
    public function createNewUser(array $data) {
        $user = new User();
        $user->setEmail($data['email']);
        $user->setPassport($data['passport']);
        $password = md5($data['password']).md5($data['password'].'2');
        $user->setPassword($password);

        return $user;
    }
}
