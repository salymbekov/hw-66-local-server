<?php

namespace App\Model\Api;


use App\Entity\User;
use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Tests\CacheWarmer\testRouterInterfaceWithoutWarmebleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT = '/client';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{passport}/{email}';
    const ENDPOINT_CHECK_CONCRETE_CLIENT = '/check-client/{passport}/{email}';
    const ENDPOINT_GET_CLIENT = '/return-client/{passport}/{email}';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT, [
                'passport' => $passport,
                'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $password
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function checkUserExist(string $password, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CONCRETE_CLIENT, [
                'passport' => $password,
                'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $email
     *
     * @param string $password
     * @return mixed
     * @throws ApiException
     */
    public function GetClient(string $email, string $password)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_GET_CLIENT, [
                'email' => $email,
                'passport' => $password
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT, self::METHOD_POST, $data);
    }
}
