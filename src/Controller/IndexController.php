<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class IndexController extends Controller
{
    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    ) {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $data = [
                        'email' => $user->getEmail(),
                        'passport' => $user->getPassport(),
                        'password' => $user->getPassword()
                    ];

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("home");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/", name="home")
     *
     */
    public function indexAction()
    {
        return $this->render('index.html.twig');
    }



    /**
     * @Route("/auth", name="auth")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param ApiContext $apiContext
     * @param UserHandler $userHandler
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function authAction(
        UserRepository $userRepository,
        Request $request,
        ApiContext $apiContext,
        UserHandler $userHandler,
        ObjectManager $manager
    ) {
        $error = null;
        $user = new User();
        $form = $this->createForm('App\Form\AuthType', $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = md5($user->getPassword()) . md5($user->getPassword() . '2');
            $email = $user->getEmail();
            $user = $userRepository->FindByEmailAndPassword($email, $password);
            if ($user) {
                $this->auth($user);
                return $this->redirectToRoute('home');
            } else {
                try {
                    if ($apiContext->checkUserExist($password, $email)) {
                        $user_data = $apiContext->GetClient($email, $password);
                        dump($user_data['password']);
                        $user = $userHandler->createNewUser($user_data);
                        $user->setPassword($user_data['password']);
                        $manager->persist($user);
                        $manager->flush();
                        dump($user);
                        $this->auth($user);
                        return $this->redirectToRoute('home');
                    } else {
                        $error = 'Error: Вы не зарегистрированы. Вам нужно пройти регистрацию.';
                    }
                } catch (ApiException $e) {

                }
            }
        }

        return $this->render('auth.html.twig', [
            'form' => $form->createView(),
            'error'=>$error

        ]);
    }

    /**
     * @param $user
     */
    public function auth($user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }

}
